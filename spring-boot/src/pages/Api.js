import React, { useEffect, useState } from "react";
// import UserContext from "../contexts/UserContext";
import axios from "axios";
import DisplayRates from "../components/DisplayRates";

const API = () => {
  const [text, setText] = useState("");

  useEffect(() => {
    axios({
      method: "get",
      url: "https://localhost:8443/api/rates",
    })
      .then(function (response) {
        console.log(response.data);
        setText(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  return (
    <div>
      <h1>API</h1>
      <DisplayRates {...text} />
    </div>
  );
};

export default API;
