import React, { useEffect, useState } from "react";
// import UserContext from "../contexts/UserContext";
import axios from "axios";
import Input from "../components/Input";
import DisplayUser from "../components/DisplayUser";
import DisplayProduct from "../components/DisplayProduct";
import styled from "styled-components";

const StyledButtons = styled.button`
  color: white;
  background-color: #3d005c;
  box-shadow: 0px 0px 0px transparent;
  border: 0px solid transparent;
  text-shadow: 0px 0px 0px transparent;
  border-radius: 25px;
  padding: 6px;
`;
const Users = () => {
  const [users, setUsers] = useState([]);
  const [products, setProducts] = useState([]);
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(null);

  useEffect(() => {
    axios({
      method: "get",
      url: "https://localhost:8443/users",
    })
      .then(function (response) {
        console.log(response.data);
        setUsers(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  useEffect(() => {
    axios({
      method: "get",
      url: "https://localhost:8443/produits",
    })
      .then(function (response) {
        console.log(response.data);
        setProducts(response.data);
      })
      .catch(function (error) {
        console.log(error);
      });
  }, []);

  const add = (userId, productId) => {
    axios
      .post(
        "https://localhost:8443/user/" +
          userId +
          "/addProductToFav/" +
          productId
      )
      .then(function (response) {
        console.log("ADDED");
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  const remove = (userId, productId) => {
    axios
      .post(
        "https://localhost:8443/user/" +
          userId +
          "/removeProductToFav/" +
          productId
      )
      .then(function (response) {
        console.log("REMOVED");
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  const onSubmit = () => {
    setError(null);
    if (!name || setName === "") {
      setError({ field: "email", message: "Please enter your email" });
    } else if (!password || password === "") {
      setError({ field: "password", message: "Please enter your password" });
    } else {
      // Check login
      add(name, password);

      console.log("User info properly entered");
    }
  };
  const onRemove = () => {
    setError(null);
    if (!name || setName === "") {
      setError({ field: "email", message: "Please enter your email" });
    } else if (!password || password === "") {
      setError({ field: "password", message: "Please enter your password" });
    } else {
      // Check login
      remove(name, password);

      console.log("User info properly entered");
    }
  };

  return (
    <div>
      <h2>Users</h2>
      <ul>
        {users.map((user) => (
          <DisplayUser key={user.id} {...user} />
        ))}
      </ul>
      <h2>Products</h2>
      {products.map((product) => (
        <div>
          <DisplayProduct key={product.id} {...product} />
        </div>
      ))}
      <h3>Add favorite item to user</h3>
      <Input
        type={"text"}
        value={name}
        setValue={setName}
        label="UserId"
        error={error?.field === "name"}
      />
      <Input
        type={"text"}
        value={password}
        setValue={setPassword}
        label="ProductId"
        error={error?.field === "password"}
      />
      {error && <div>Error: {error.message}</div>}
      <StyledButtons onClick={onSubmit}>Add Item</StyledButtons>
      <StyledButtons onClick={onRemove}>Remove Item</StyledButtons>
    </div>
  );
};

export default Users;
