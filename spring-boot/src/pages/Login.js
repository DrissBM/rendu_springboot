import React, { useState } from "react";
import Input from "../components/Input";
import { Link } from "react-router-dom";
// import UserContext from "../contexts/UserContext";
import axios from "axios";
import styled from "styled-components";

const StyledButtons = styled.button`
  color: white;
  background-color: #3d005c;
  box-shadow: 0px 0px 0px transparent;
  border: 0px solid transparent;
  text-shadow: 0px 0px 0px transparent;
  border-radius: 25px;
  padding: 6px;
`;

const Login = () => {
  const [connected, setconnected] = useState(false);
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(null);

  const login = (name, password) => {
    axios
      .post("https://localhost:8443/user/login", {
        name: name,
        password: password,
      })
      .then(function (response) {
        if (response.data === true) {
          setconnected(true);
        } else {
          setconnected(false);
        }
      })
      .catch(function (error) {
        console.log(error, "aaaa");
      });
  };

  /**
   * Validate form and retrieve calls the getJWT function
   */
  const onSubmit = () => {
    setError(null);
    if (!name || setName === "") {
      setError({ field: "email", message: "Please enter your email" });
    } else if (!password || password === "") {
      setError({ field: "password", message: "Please enter your password" });
    } else {
      // Check login
      login(name, password);

      console.log("User info properly entered");
    }
  };

  return (
    <div>
      <h1>Login</h1>
      <p>{connected.toString()}</p>
      <Input
        type={"text"}
        value={name}
        setValue={setName}
        label="name"
        error={error?.field === "name"}
      />
      <Input
        type={"password"}
        value={password}
        setValue={setPassword}
        label="*********"
        error={error?.field === "password"}
      />
      {error && <div>Error: {error.message}</div>}
      <StyledButtons onClick={onSubmit}>Submit</StyledButtons>
      <Link to="/register">Sign Up</Link>
    </div>
  );
};

export default Login;
