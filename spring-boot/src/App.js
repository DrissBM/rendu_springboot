import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
import Home from "./pages/Home";
import Login from "./pages/Login";
import API from "./pages/Api";
import Register from "./pages/Register";
import Users from "./pages/Users";
import { Link } from "react-router-dom";

const App = () => {
  return (
    <Router>
      <div className="flex">
        <Link to="/">Home</Link>
        <Link to="/login">Login</Link>
        <Link to="/register">Register</Link>
        <Link to="/api">API</Link>
        <Link to="/users">Users</Link>
      </div>
      <Switch>
        <>
          <Route path="/" exact component={Home} />
          <Route path="/login" exact component={Login} />
          <Route path="/api" exact component={API} />
          <Route path="/register" exact component={Register} />
          <Route path="/users" exact component={Users} />
          {/* <Route path="/user/:userId" exact component={UserProfil} /> */}
        </>
      </Switch>
    </Router>
  );
};

export default App;
