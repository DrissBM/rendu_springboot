import React from "react";
import Rate from "./Rate";

const Input = ({ base, date, rates }) => (
  <>
    {base}
    {date}
    <Rate {...rates} />
  </>
);

export default Input;
