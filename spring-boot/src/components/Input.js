import React from "react";

const Input = ({ value, setValue, label, type, error }) => (
  <>
    <label htmlFor={label}>{label} : </label>
    <input
      value={value}
      onChange={(e) => setValue(e.target.value)}
      placeholder={label}
      type={type}
      id={label}
      style={{ borderColor: error ? "red" : "black" }}
    />
  </>
);

export default Input;
