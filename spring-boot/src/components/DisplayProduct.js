import React from "react";
// import Rate from "./Rate";

const DisplayProduct = ({ id, category, nom, prix }) => (
  <li key={id}>
    {id}, {category}, {nom}, {prix}
  </li>
);

export default DisplayProduct;
