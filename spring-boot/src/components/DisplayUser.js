import React from "react";
import DisplayProduct from "../components/DisplayProduct";

const DisplayUser = ({ id, name, favouriteProducts }) => (
  <li key={id}>
    <div>
      {id}, {name}
    </div>
    <ul>
      FavProduct :
      {favouriteProducts.map((product) => (
        <li>
          <DisplayProduct key={product.id} {...product} />
        </li>
      ))}
    </ul>
  </li>
);

export default DisplayUser;
