import React from "react";

const Rate = (props) => (
  <>
    {Object.keys(props).map((key) => (
      <option value={key}>
        {key} : {props[key]}
      </option>
    ))}
  </>
);

export default Rate;
