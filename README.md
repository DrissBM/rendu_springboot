# Getting Started

### Folders

For further reference, please consider the following sections:

- The folder [/spring-boot](./spring-boot) is the front-end
- The folder [/SpringBootHello](./spring-boot) is the back-end

### Front-end

To start the front-end:

- cd /spring-boot
- npm i
- npm start

### Back-end

Run /SpringBootHello/src/test/java/com/crea/backend/SpringBootHelloSpringBootHelloApplicationTests.java with jdk 11
