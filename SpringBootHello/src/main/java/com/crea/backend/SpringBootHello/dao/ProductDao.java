package com.crea.backend.SpringBootHello.dao;

import java.util.List;

import com.crea.backend.SpringBootHello.model.Product;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

@Repository
public interface ProductDao extends JpaRepository<Product, Integer> {

    List<Product> findAll();

    Product findById(int id);

    Product save(Product p);

    List<Product> findProductByPrixGreaterThan(int prix);
}