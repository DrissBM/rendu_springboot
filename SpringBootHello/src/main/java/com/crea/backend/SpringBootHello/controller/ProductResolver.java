package com.crea.backend.SpringBootHello.controller;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.crea.backend.SpringBootHello.dao.ProductDao;
import com.crea.backend.SpringBootHello.model.Product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProductResolver implements GraphQLQueryResolver {

    @Autowired
    private ProductDao productDao;

    public Product findById(int id) {
        return productDao.findById(id);
    }

}
