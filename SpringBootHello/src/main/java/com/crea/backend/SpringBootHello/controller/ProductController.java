package com.crea.backend.SpringBootHello.controller;

import java.util.List;

import com.crea.backend.SpringBootHello.dao.ProductDao;
import com.crea.backend.SpringBootHello.model.Product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

//API REST
@CrossOrigin
@Api(description = "Call our products")
@RestController
public class ProductController {

    // produit/{id}

    // Get request sends id to path
    @ApiOperation(value = "Get a products by his id")
    @GetMapping(value = "/produit/{id}")
    public Product DisplayProduct(@PathVariable int id) {
        return productDao.findById(id);
    }

    @Autowired
    private ProductDao productDao;

    // Récupérer la liste des produits
    @ApiOperation(value = "Get all the products")
    @RequestMapping(value = "/produits", method = RequestMethod.GET)
    public List<Product> listeProduits() {
        return productDao.findAll();
    }

    // Récupérer la liste des produits
    @ApiOperation(value = "Get products more expensive than <pice>")
    @RequestMapping(value = "/produits/{price}", method = RequestMethod.GET)
    public List<Product> produitsGreaterThan(@PathVariable int price) {
        return productDao.findProductByPrixGreaterThan(price);
    }

    // Add product
    @ApiOperation(value = "Add a product")
    @PostMapping(value = "/produits")
    public void ajouterProduit(@RequestBody Product product) {
        productDao.save(product);
    }

    // Add product
    // @PostMapping(value = "/produits1")
    // public ResponseEntity<Void> addProduit(@RequestBody Product product) {
    // //TODO:
    // return null;
    // }

    @ApiOperation(value = "Delete a product")
    @DeleteMapping(value = "/produit/{id}")
    public void DeleteProduct(@PathVariable int id) {
        productDao.delete(productDao.findById(id));
    }

}
