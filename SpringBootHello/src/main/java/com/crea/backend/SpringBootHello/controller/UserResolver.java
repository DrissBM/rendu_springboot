package com.crea.backend.SpringBootHello.controller;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.crea.backend.SpringBootHello.dao.UserDao;
import com.crea.backend.SpringBootHello.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserResolver implements GraphQLQueryResolver {
    @Autowired
    private UserDao userDao;

    public User findUserById(int id) {
        return userDao.findById(id);
    }
}
