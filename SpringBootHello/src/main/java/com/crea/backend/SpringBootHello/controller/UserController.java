package com.crea.backend.SpringBootHello.controller;

import com.crea.backend.SpringBootHello.dao.ProductDao;
import com.crea.backend.SpringBootHello.dao.UserDao;
import com.crea.backend.SpringBootHello.model.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@Api(description = "User management")
@RestController
public class UserController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private ProductDao productDao;

    // Get request sends id to path
    @ApiOperation(value = "Display a specific User")
    @GetMapping(value = "/user/{id}")
    public User displayUser(@PathVariable int id) {
        return userDao.findById(id);
    }

    // Récupérer la liste des users
    @ApiOperation(value = "Display all Users")
    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<User> listUsers() {
        return userDao.findAll();
    }

    // Add User
    @ApiOperation(value = "Add User")
    @CrossOrigin
    @PostMapping(value = "/users")
    public void addUser(@RequestBody User user) {
        userDao.save(user);
    }

    // to login the user we check username - password
    @ApiOperation(value = "Login User")
    @CrossOrigin
    @PostMapping(value = "/user/login")
    public boolean login(@RequestBody User user) {

        // We retrieve the corresponding user
        User usrCheck = userDao.findByNameAndPassword(user.getName(), user.getPassword());

        // Verify that the usr to check exists
        if (usrCheck != null) {
            return true;
        }

        return false;
    }

    /**
     * Add Product to Fav
     *
     * @param productId product Id
     * @param id        user Id
     */
    @ApiOperation(value = "Add a product to favourite")
    @PostMapping(value = "/user/{id}/addProductToFav/{productId}")
    public void addProductToFavourite(@PathVariable int productId, @PathVariable int id) {
        User user = userDao.findById(id);
        user.addFav(productDao.findById(productId));
        userDao.save(user);
    }

    /**
     * Remove Product from fav
     *
     * @param productId product Id
     * @param id        user Id
     */
    @ApiOperation(value = "Remove product from favourite")
    @PostMapping(value = "/user/{id}/removeProductToFav/{productId}")
    public void removeProductToFavourite(@PathVariable int productId, @PathVariable int id) {
        User user = userDao.findById(id);
        user.removeFav(productDao.findById(productId));
        userDao.save(user);
    }

    @ApiOperation(value = "Delete user")
    @DeleteMapping(value = "/user/{id}")
    public void deleteUser(@PathVariable int id) {
        userDao.delete(userDao.findById(id));
    }

}
