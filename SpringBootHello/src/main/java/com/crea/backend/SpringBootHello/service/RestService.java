package com.crea.backend.SpringBootHello.service;

import com.crea.backend.SpringBootHello.json.RatesResponse;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RestService {

    private final RestTemplate restTemplate;

    public RestService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public String getAPIPlainJSON(String url) {
        return this.restTemplate.getForObject(url, String.class);
    }

    public RatesResponse getRatesAsObject() {
        String url = "https://api.exchangeratesapi.io/latest?base=USD";
        return this.restTemplate.getForObject(url, RatesResponse.class);
    }
}