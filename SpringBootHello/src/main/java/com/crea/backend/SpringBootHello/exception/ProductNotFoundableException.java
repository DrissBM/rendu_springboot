package com.crea.backend.SpringBootHello.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ProductNotFoundableException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public ProductNotFoundableException(String s) {
        super(s);
    }
}
