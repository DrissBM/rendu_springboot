package com.crea.backend.SpringBootHello.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    // @Length(min=3,max=20,message="Trop long ou trop court!")
    private String name;
    private String password;

    // User can have favorite products
    @ManyToMany
    private List<Product> favouriteProducts;

    public User(int id, String name, String password, List<Product> favouriteProducts) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.favouriteProducts = favouriteProducts;
    }

    public User(int id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }

    public User() {

    }

    public int getId() {
        return id;
    }

    public List<Product> getFavouriteProducts() {
        return favouriteProducts;
    }

    public void setFavouriteProducts(List<Product> favouriteProducts) {
        this.favouriteProducts = favouriteProducts;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void addFav(Product p) {
        if (!this.favouriteProducts.contains(p)) {
            this.favouriteProducts.add(p);
        }
    }

    public void removeFav(Product p) {
        if (this.favouriteProducts.contains(p)) {
            this.favouriteProducts.remove(p);
        }
    }
}
