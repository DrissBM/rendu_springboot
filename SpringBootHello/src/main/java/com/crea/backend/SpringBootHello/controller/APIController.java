package com.crea.backend.SpringBootHello.controller;

import com.crea.backend.SpringBootHello.json.RatesResponse;
import com.crea.backend.SpringBootHello.service.RestService;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

//API REST
@CrossOrigin
@Api(description = "Appel des API externes")
@RestController
@RequestMapping(path = "/api")
public class APIController {

    @ApiOperation(value = "Get all the rates")
    @GetMapping(value = "/rates")
    public String GetCurrencyRates() {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        RestService restService = new RestService(restTemplateBuilder);
        return restService.getAPIPlainJSON("https://api.exchangeratesapi.io/latest?base=USD");
    }

    @ApiOperation(value = "Get all the rates as an Object")
    @GetMapping(value = "/ratesAsObject")
    public RatesResponse GetCurrencyRatesAsObject() {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        RestService restService = new RestService(restTemplateBuilder);
        return restService.getRatesAsObject();
    }

    @ApiOperation(value = "Get a random joke about Chuck Norris")
    @GetMapping(value = "/chuck")
    public String ChuckJoke() {
        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
        RestService restService = new RestService(restTemplateBuilder);
        return restService.getAPIPlainJSON("https://api.chucknorris.io/jokes/random");
    }

}
