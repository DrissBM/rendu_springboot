package com.crea.backend.SpringBootHello.dao;

import java.util.List;

import com.crea.backend.SpringBootHello.model.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {

    List<User> findAll();

    User findById(int id);

    User findByNameAndPassword(String name, String password);

    User save(User u);

}
